USE [studentdb]
GO
/****** Object:  Table [dbo].[tabstudent]    Script Date: 12/29/2017 7:42:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tabstudent](
	[id] [nvarchar](10) NOT NULL,
	[name] [nvarchar](150) NULL,
	[diem1] [float] NULL,
	[diem2] [float] NULL,
	[diem3] [float] NULL,
	[dtong] [float] NULL,
	[xeploai] [nvarchar](30) NULL,
 CONSTRAINT [PK_tabstudent] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[tabstudent] ([id], [name], [diem1], [diem2], [diem3], [dtong], [xeploai]) VALUES (N'02a', N'hahshs', 9, 6, 7, 22, N'Đạt')
INSERT [dbo].[tabstudent] ([id], [name], [diem1], [diem2], [diem3], [dtong], [xeploai]) VALUES (N'1', N'CSS', 8, 8, 7, 23, N'Đạt')
INSERT [dbo].[tabstudent] ([id], [name], [diem1], [diem2], [diem3], [dtong], [xeploai]) VALUES (N'2', N'kaksd', 5, 4, 5, 14, N'Không đạt')
INSERT [dbo].[tabstudent] ([id], [name], [diem1], [diem2], [diem3], [dtong], [xeploai]) VALUES (N'654', N'rr', 5, 6, 5, 16, N'Đạt')
INSERT [dbo].[tabstudent] ([id], [name], [diem1], [diem2], [diem3], [dtong], [xeploai]) VALUES (N'gh32', N'hah', 5, 4, 9, 18, N'Đạt')
INSERT [dbo].[tabstudent] ([id], [name], [diem1], [diem2], [diem3], [dtong], [xeploai]) VALUES (N'ST001', N'XXX', 5, 5, 5, 15, N'Không đạt')
INSERT [dbo].[tabstudent] ([id], [name], [diem1], [diem2], [diem3], [dtong], [xeploai]) VALUES (N'ST002', N'ACB', 9, 9, 9, 18, N'Đạt')
