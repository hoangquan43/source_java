package cau2;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

import cau1.Students;
import cau3.DBStudents;

public class MainApp {
	private int maxNum;
	private int sNum;
	private ArrayList<Students> sList = new ArrayList<>();
	private DBStudents studentDao = new DBStudents();
	private DBStudents DAO = new DBStudents();
	
	
	/**
	 * Bây giờ tạo con tructor
	 */
	public MainApp() {
	}
	
	public MainApp(int maxNum, int sNum) {
		this.maxNum = maxNum;
		this.sNum = sNum;
	}
	/*
	 *  Seter và getter. cái này có cũng được mà không có cũng được. 
	 *  Đề yêu cầu thì làm mà không yêu cầu thì thôi.Khỏi làm nhằm rút ngắn thời gian.
	 */

	public int getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(int maxNum) {
		this.maxNum = maxNum;
	}

	public int getsNum() {
		return sNum;
	}

	public void setsNum(int sNum) {
		this.sNum = sNum;
	}
	
	/*
	 * Bây giờ làm casci method initStudents(int n)
	 */
	
	
	public void initStudents(int n) {
		/*
		 * Tạo danh sách Student có n bản ghi thì dùng for bình thường mà thêm thôi.
		 * 
		 * Ở đây nhớ đọc kĩ đề nhé.
		 * Đề có chứa trương maxNum xác định kích thước lơn nhất. mà sNum lưu danh sách student.
		 * Ở cái này cần tư duy một tí.
		 * 
		 * Ở đây có hai trương hợp:
		 * 
		 * TH1:n < maxNum || n == maxNum 
		 * TH2: n > maxNum
		 * Ở TH2 chúng ta không để cảy ra trường hợp này.
		 * ví dụ:
		 *  1 danh sách kích thước lớn nhất là 5 .
		 *  mà ta tạo 7 bản ghi đi.
		 *  
		 *   nếu như vậy các bạn có thấy vô lí không.
		 *   -- Chúng ta cần lưu ý nhé:
		 *   
		 *    chúng ta có thể làm như sau(đây là cách của mình nhé, còn nhiều cách khác.):
		 *   ở đay chúng ta cần dựa mà cái trường sNum
		 *   cứ mỗ lần add vào list thì sNum nó tăng lên đến khi nào nó bằng kích thước thì break.
		 *    nếu n > lớn hơn maxNum
		 *    Ở ĐÂY SỬA LẠI CHO MÌNH TÍ LÀ thay sNum == n.:))))));
		 * 
		 */
		
		for (int i = 0; i < n; i++) {
			if (sNum<maxNum) {
				addStudents();
				sNum++;
			} else {
				System.out.println("Vượt quá kích thước cho phép...");
				System.exit(0);
			}
		}
	}
	
	
	/*
	 *  method addStudents
	 */
	
	public void addStudents() {
		// Khởi tạo một đối tượng Student sau đó nhập vào.
		/*
		 *  Ở trong classs Student có method inputData() đó.
		 */
	
		try {
			Students st = new Students();
			st.inputData();
			DAO.exportToDB(st);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Sau đó add cái st này vào sList;
		//sList.add(st);
		
		
		
		
	}
	/*
	 * Hiền thị dach sách trong list ra.
	 * 
	 * cái này chắc các bạn đã quen thuộc rồi.
	 * 
	 */
	
	public void display() {
		try {
			sList = DAO.importFromDB();
			for (Students students : sList) {
				System.out.println(students);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * Bây giờ method cuối cùng..
	 * là tìm tinh viên theo name.
	 * cái này cũng đơn giản thôi.:)))
	 */
	
	public void seachStudetnsByName() {
		int dem =0;
		System.out.println("Nhập sinh viên cần tìm kiếm: ");
		String name = new Scanner(System.in).nextLine(); 
		for (Students item : sList) {
			if (item.getName().contains(name)) {
				dem++;
				System.out.println(item);
			}
		}
		if (dem==0) {
			System.out.println("Khong tôn tại sinh viên trong danh sách.");
		} 
	}
	
	// Bây giờ tử teets nhé.
	
	public static void main(String[] args) {
		
	//	MainApp ma = new MainApp(maxNum, sNum);
		// Ở đây ình sẽ tạo kích thước lớn nhất là 5 mà danh sách hiện tại chưa có thằng đéo nào nên
		// sNum =0 nhé
		/*
		 *  
		 */
		
		MainApp ma = new MainApp(3, 0);
		System.out.println("Nhập số bản ghi: ");
		int num = new Scanner(System.in).nextInt(); 
		ma.initStudents(num);
		ma.display();
		ma.seachStudetnsByName();
		
		/*
		 * Ở dây sanh sách lớn nhất là 5 mà chỉ mới nhập dc 2 nến trường hhowpj này OK.
		 * Bây giờ thứ nhập số bản ghi lớn hơn 5 để xem nhé.
		 * Ở đây nhập 5 lâu quá nên sửa lại đi.
		 * 
		 * maxNum == 2
		 * maxNum n =3
		 * Ok.Ở đây mính dính phải lỗi hơi ngu ngu xíu.:)))
		 * 
		 */
	}
	
	

	
	
	

	
	
}
