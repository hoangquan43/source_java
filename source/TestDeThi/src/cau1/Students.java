package cau1;

import java.util.Scanner;

public class Students {
	// Khai báo các trường

	private String id, name, xepLoai;
	private float diem1, diem2, diem3, dtong;

	// Tạo Contructor
	public Students() {

	}

	public Students(String id, String name, float diem1, float diem2, float diem3) {
		this.id = id;
		this.name = name;
		this.diem1 = diem1;
		this.diem2 = diem2;
		this.diem3 = diem3;
	}

	// Tạo Getter, Setter

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getXepLoai() {
		return xepLoai;
	}

	public void setXepLoai(String xepLoai) {
		this.xepLoai = xepLoai;
	}

	public float getDiem1() {
		return diem1;
	}

	public void setDiem1(float diem1) {
		this.diem1 = diem1;
	}

	public float getDiem2() {
		return diem2;
	}

	public void setDiem2(float diem2) {
		this.diem2 = diem2;
	}

	public float getDiem3() {
		return diem3;
	}

	public void setDiem3(float diem3) {
		this.diem3 = diem3;
	}

	public float getDtong() {
		return dtong;
	}

	public void setDtong(float dtong) {
		this.dtong = dtong;
	}

	public void inputData() {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập id: ");
		this.id = sc.nextLine();
		System.out.println("Nhập Tên: ");
		this.name = sc.nextLine();
		System.out.println("Nhập điểm 1: ");
		this.diem1 = sc.nextFloat();
		System.out.println("Nhập điểm 2: ");
		this.diem2 = sc.nextFloat();
		System.out.println("Nhập điểm 3: ");
		this.diem3 = sc.nextFloat();
	}

	// Phương thức ToString
	@Override
	public String toString() {
		doGrade();
		return this.id + "\t" + this.name + "\t" + this.diem1 + "\t" + this.diem2 + "\t" + this.diem3 + "\t" + this.getDtong()+"\t"+this.getXepLoai();
	}

	public void doGrade() {
		float diemtb = this.diem1 + this.diem2 + this.diem3;
		this.setDtong(diemtb);
		if (getDtong()>15) {
			setXepLoai("Đạt");
		}else {
			setXepLoai("Không đạt");
		}

		

	}

	public void dislay() {
		doGrade();
		System.out.println(
				this.id + "\t" + this.name + "\t" + this.diem1 + "\t" + this.diem2 + "\t" + this.diem3 + "\t" + this.getDtong()+"\t"+this.getXepLoai());
	}

}
