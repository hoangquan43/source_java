package cau3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import cau1.Students;

public class DBStudents {
	private Connection conn = null;
	private Statement state = null;
	private ResultSet rs = null;

	public Connection getConnect() {
		String url = "jdbc:sqlserver://localhost:1433;databaseName= studentdb";
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerConnection");
			try {
				conn = DriverManager.getConnection(url, "sa", "........");
				return conn;
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return conn;
	}

	public boolean exportToDB(Students st) throws SQLException {
		st.doGrade();
		String query = "INSERT INTO dbo.tabstudent   VALUES('" + st.getId() + "',N'" + st.getName() + "','"
				+ st.getDiem1() + "','" + st.getDiem2() + "','" + st.getDiem3() + "','" + st.getDtong() + "',N'"
				+ st.getXepLoai() + "');";

		state = getConnect().createStatement();
		int row = state.executeUpdate(query);
		if (row != 0) {
			return true;
		}
		return false;

	}

	public ArrayList<Students> importFromDB() throws SQLException {
		ArrayList<Students> listStudents = new ArrayList<>();

		String query = "SELECT * FROM dbo.tabstudent;";
		state = getConnect().createStatement();
		rs = state.executeQuery(query);
		while (rs.next()) {
			Students st = new Students();
			st.setId(rs.getString(1));
			st.setName(rs.getString(2));
			st.setDiem1(rs.getFloat(3));
			st.setDiem2(rs.getFloat(4));
			st.setDiem3(rs.getFloat(5));
			st.setDtong(rs.getFloat(6));
			st.setXepLoai(rs.getString(7));

			listStudents.add(st);

		}

		return listStudents;
	}

}
